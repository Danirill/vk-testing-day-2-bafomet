package com.travels.searchtravels

import android.net.Uri
import android.provider.MediaStore
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.travels.searchtravels.activity.ChipActivity
import com.travels.searchtravels.activity.MainActivity
import com.travels.searchtravels.utils.Constants
import kotlinx.android.synthetic.main.header3.*
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import java.io.File


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun createActivity() {
        val scenario = launch(MainActivity::class.java)
        scenario.moveToState(Lifecycle.State.CREATED)
        scenario.moveToState(Lifecycle.State.RESUMED)
    }

    // Пункт задания №1
    // Проверка на правильность реагирование на категории загруженных изображений
    @Test
    fun checkSea() {
        val myuri: Uri? = Uri.fromFile(File("src/test/java/com/travels/searchtravels/testimages/sea.jpeg"));
        val scenario = launch(MainActivity::class.java)
        scenario.onActivity { activity ->
            activity.uploadImage(myuri);
        }
        Assert.assertEquals("Rimini", Constants.PICKED_CITY_EN);
        Assert.assertEquals("Римини", Constants.PICKED_CITY_RU);
    }

    @Test
    fun checkOcean() {
        val myuri: Uri? = Uri.fromFile(File("src/test/java/com/travels/searchtravels/testimages/ocean.jpg"));
        val scenario = launch(MainActivity::class.java)
        scenario.onActivity { activity ->
            activity.uploadImage(myuri);
        }
        Assert.assertEquals("Rimini", Constants.PICKED_CITY_EN);
        Assert.assertEquals("Римини", Constants.PICKED_CITY_RU);
    }

    @Test
    fun checkBeach() {
        val myuri: Uri? = Uri.fromFile(File("src/test/java/com/travels/searchtravels/testimages/beach.jpeg"));
        val scenario = launch(MainActivity::class.java)
        scenario.onActivity { activity ->
            activity.uploadImage(myuri);
        }
        Assert.assertEquals("Rimini", Constants.PICKED_CITY_EN);
        Assert.assertEquals("Римини", Constants.PICKED_CITY_RU);
    }

    @Test
    fun checkMountains() {
        val myuri: Uri? = Uri.fromFile(File("src/test/java/com/travels/searchtravels/testimages/mountain.jpg"));
        val scenario = launch(MainActivity::class.java)
        scenario.onActivity { activity ->
            activity.uploadImage(myuri);
        }
        Assert.assertEquals("Sochi", Constants.PICKED_CITY_EN);
        Assert.assertEquals("Сочи", Constants.PICKED_CITY_RU);
    }

    @Test
    fun checkSnow() {
        val myuri: Uri? = Uri.fromFile(File("src/test/java/com/travels/searchtravels/testimages/snow.jpg"));
        val scenario = launch(MainActivity::class.java)
        scenario.onActivity { activity ->
            activity.uploadImage(myuri);
        }
        Assert.assertEquals("Helsinki", Constants.PICKED_CITY_EN);
        Assert.assertEquals("Хельсинки", Constants.PICKED_CITY_RU);
    }

    // Пункт задания №2
    // Проверка на корректность получаемых через API данных о цене путешествия
    @Test
    fun checkTicketPriceNotEmpty() {
        val scenario = launch(ChipActivity::class.java)
        var ticketPrice: String = "";
        scenario.onActivity { activity ->
            activity.getInfoNomad("Sochi")
            ticketPrice = activity.airticketTV.text.toString();
        }

        var floatPrice: Float? = ticketPrice.substring(2).substring(0, ticketPrice.length - 2).toFloatOrNull();
        Assert.assertNotNull(ticketPrice);
    }

    @Test
    fun checkTicketPriceNotNegative() {
        val scenario = launch(ChipActivity::class.java)
        var ticketPrice: String = "";
        scenario.onActivity { activity ->
            activity.getInfoNomad("Sochi")
            ticketPrice = activity.airticketTV.text.toString();
        }

        var floatPrice: Float? = ticketPrice.substring(2).substring(0, ticketPrice.length - 2).toFloatOrNull();
        if (floatPrice != null) {
            Assert.assertTrue(floatPrice > 0)
        };
    }

    // Пункт задания №3
    // Проверка на корректность получаемых через API данных о распознанном изображении
    @Test
    fun checkRome() {
        val myuri: Uri? = Uri.fromFile(File("src/test/java/com/travels/searchtravels/testimages/rome.jpg"));
        val scenario = launch(MainActivity::class.java)
        scenario.onActivity { activity ->
            activity.uploadImage(myuri);
        }
        Assert.assertEquals("Rome", Constants.PICKED_CITY_EN);
        Assert.assertEquals("Рим", Constants.PICKED_CITY_RU);
    }

    @Test
    fun checkEmpty() {
        val myuri: Uri? = Uri.fromFile(File("src/test/java/com/travels/searchtravels/testimages/mem.jpeg"));
        val scenario = launch(MainActivity::class.java)
        scenario.onActivity { activity ->
            activity.uploadImage(myuri);
        }
        Assert.assertEquals(null, Constants.PICKED_CITY_EN);
        Assert.assertEquals(null, Constants.PICKED_CITY_RU);
    }



}